$identity = $args[0]
$number = $args[1]
Set-ADUser -Identity $identity -Replace @{ipPhone=$number}
Get-ADUser  -Properties ipPhone -Identity $identity | select samaccountname,ipPhone