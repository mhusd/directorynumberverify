# PURPOSE

Convenience tool for checking a user's extension.

# SYNOPSIS

    get <samaccountname>
    set <samaccountname> <extension>

# DESCRIPTION

`Get` queries AD by `samaccountname`. Requires the EXACT `samaccountname`. `Set` will change the `ipPhone` field of the specified account with `<extension>`. Set will also query the account again so the user can immediately verify the results.

